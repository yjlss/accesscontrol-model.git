# -*- coding: utf-8 -*-
# !/usr/bin/env python

"""
利用python实时获取tcpdump的监控数据，并将其发送给服务端：
在网络结构中，主要是监听as1-ovs1-eth2网卡的网络数据信息，也就是监听as1-c2和as1-c3的访问信息。
通过抓包就可以知道服务端到客户端的访问，并通过本系统的处理，就可以实现访问认证。
"""

import subprocess
import socket
import time


class GrabBag:
    def __init__(self, ipaddr, port):
        self.ipaddr = ipaddr
        self.port = port

    def py_tcpdump_as1(self):
        c1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        c1.connect((self.ipaddr, self.port))
        # 抓目的主机为10.0.3.1或者10.0.3.2(两个服务端)的tcp(http)连接，as1-ovs1-eth2是客户端到服务端的必经网卡
        proc = subprocess.Popen('tcpdump dst host 10.0.3.1 or 10.0.3.2 -l -i as1-ovs1-eth2 and tcp -n',
                                stdout=subprocess.PIPE, shell=True)

        while True:
            line = proc.stdout.readline().decode(encoding='utf-8')
            line = line.strip()
            if not line:
                print('tcpdump finished...')
                break
            print(line)
            c1.send(line.encode())
            time.sleep(0.3)


if __name__ == '__main__':
    grab = GrabBag('10.0.2.15', 62121)
    grab.py_tcpdump_as1()
