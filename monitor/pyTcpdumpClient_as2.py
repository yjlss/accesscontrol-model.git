# -*- coding: utf-8 -*-
# !/usr/bin/env python

import subprocess
import socket
import time


class GrabBag:
    def __init__(self, ipaddr, port):
        self.ipaddr = ipaddr
        self.port = port

    def py_tcpdump_as1(self):
        c1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        c1.connect((self.ipaddr, self.port))
        proc = subprocess.Popen('tcpdump dst host 10.0.3.1 or 10.0.3.2 -l -i as2-ovs1-eth2 and tcp -n', stdout=subprocess.PIPE, shell=True)

        while True:
            line = proc.stdout.readline().decode(encoding='utf-8')
            line = line.strip()
            if not line:
                print('tcpdump finished...')
                break
            print(line)
            c1.send(line.encode())
            time.sleep(0.3)


if __name__ == '__main__':
    grab = GrabBag('10.0.2.15', 62121)
    grab.py_tcpdump_as1()
