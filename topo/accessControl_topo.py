#!/usr/bin/env python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

def myNetwork():

    net = Mininet( topo=None,
                   build=False,
                   ipBase='10.0.0.0/8')

    info( '*** Adding controller\n' )
    as1-ctrl1=net.addController(name='as1-ctrl1',
                      controller=RemoteController,
                      ip='172.17.0.2',
                      protocol='tcp',
                      port=6633)

    as2-ctrl1=net.addController(name='as2-ctrl1',
                      controller=RemoteController,
                      ip='172.17.0.3',
                      protocol='tcp',
                      port=6633)

    info( '*** Add switches\n')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch, failMode='standalone')
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch, failMode='standalone')
    s3 = net.addSwitch('s3', cls=OVSKernelSwitch, failMode='standalone')
    s4 = net.addSwitch('s4', cls=OVSKernelSwitch, failMode='standalone')
    s5 = net.addSwitch('s5', cls=OVSKernelSwitch, failMode='standalone')
    as1-ovs1 = net.addSwitch('as1-ovs1', cls=OVSKernelSwitch, dpid='0000000000000001')
    as2-ovs1 = net.addSwitch('as2-ovs1', cls=OVSKernelSwitch, dpid='0000000000000002')
    r8 = net.addHost('r8', cls=Node, ip='0.0.0.0')
    r8.cmd('sysctl -w net.ipv4.ip_forward=1')

    info( '*** Add hosts\n')
    as1-c1 = net.addHost('as1-c1', cls=Host, ip='10.0.1.1/24', defaultRoute=None)
    as1-c2 = net.addHost('as1-c2', cls=Host, ip='10.0.1.2/24', defaultRoute=None)
    as1-c3 = net.addHost('as1-c3', cls=Host, ip='10.0.1.3/24', defaultRoute=None)
    as2-c3 = net.addHost('as2-c3', cls=Host, ip='10.0.2.3/24', defaultRoute=None)
    as2-c2 = net.addHost('as2-c2', cls=Host, ip='10.0.2.2/24', defaultRoute=None)
    as2-c1 = net.addHost('as2-c1', cls=Host, ip='10.0.2.1/24', defaultRoute=None)
    test-s1 = net.addHost('test-s1', cls=Host, ip='10.0.3.1/24', defaultRoute=None)
    test-s2 = net.addHost('test-s2', cls=Host, ip='10.0.3.2/24', defaultRoute=None)

    info( '*** Add links\n')
    net.addLink(as1-c1, s1)
    net.addLink(as1-c2, s2)
    net.addLink(s2, as1-c3)
    net.addLink(as2-c1, s4)
    net.addLink(as2-c3, s3)
    net.addLink(s3, as2-c2)
    net.addLink(test-s1, s5)
    net.addLink(test-s2, s5)
    net.addLink(s1, as1-ovs1)
    net.addLink(s2, as1-ovs1)
    net.addLink(s4, as2-ovs1)
    net.addLink(s3, as2-ovs1)
    net.addLink(as1-ovs1, r8)
    net.addLink(r8, as2-ovs1)
    net.addLink(s5, r8)

    info( '*** Starting network\n')
    net.build()
    info( '*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info( '*** Starting switches\n')
    net.get('s1').start([])
    net.get('s2').start([])
    net.get('s3').start([])
    net.get('s4').start([])
    net.get('s5').start([])
    net.get('as1-ovs1').start([as1-ctrl1])
    net.get('as2-ovs1').start([as2-ctrl1])

    info( '*** Post configure switches and hosts\n')

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()
